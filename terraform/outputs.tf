output "networks" {
  value = module.vpcs.*
}

output "instances" {
  value = module.instancies.*
}

output "buckets" {
  value = module.buckets.*
}

output "loadbalancers" {
  value = module.loadbalancers.*
}

output "functions" {
  value = module.functions.*
}