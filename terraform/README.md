# Yandex Cloud main module

This module orchestrates creation of resources in Yandex Cloud using child modules under ./modules folder

## Input variables
  None

## Usage
  Check README.md files for every child module which you interested and update main.tf configuration including nessesary resources using examples provided for each specific module, mind dependencies.

## Outputs
  All created resources details