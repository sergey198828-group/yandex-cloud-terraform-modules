module "vpcs" {
  for_each           = { for file in fileset("./data/vpcs", "*.yaml") : trimsuffix(file, ".yaml") => yamldecode(file("./data/vpcs/${file}")) }
  source             = "./modules/vpcs"
  yc_network_name    = each.key
  yc_network_subnets = each.value
}

module "instancies" {
  for_each         = { for file in fileset("./data/instancies", "*.yaml") : trimsuffix(file, ".yaml") => yamldecode(file("./data/instancies/${file}")) }
  source           = "./modules/instancies"
  yc_instance_name = each.key
  yc_instance_spec = each.value
  depends_on = [
    module.vpcs
  ]
}

module "buckets" {
  for_each       = { for file in fileset("./data/buckets", "*.yaml") : trimsuffix(file, ".yaml") => yamldecode(file("./data/buckets/${file}")) }
  source         = "./modules/buckets"
  yc_bucket_name = each.key
  yc_bucket_spec = each.value
}

module "loadbalancers" {
  for_each = {
    for file in fileset("./data/loadbalancers", "*.yaml") : trimsuffix(file, ".yaml") => yamldecode(file("./data/loadbalancers/${file}"))
  }
  source               = "./modules/loadbalancers"
  yc_loadbalancer_name = each.key
  yc_loadbalancer_spec = each.value
  depends_on = [
    module.vpcs,
    module.instancies
  ]
}

module "functions" {
  for_each         = { for file in fileset("./data/functions", "*.yaml") : trimsuffix(file, ".yaml") => yamldecode(file("./data/functions/${file}")) }
  source           = "./modules/functions"
  yc_function_name = each.key
  yc_function_spec = each.value
}