resource "yandex_iam_service_account" "function_service_account" {
  count = var.yc_function_spec.create_service_account ? 1 : 0
  name  = var.yc_function_spec.service_account_name
}

resource "yandex_function" "function" {
  name               = var.yc_function_name
  description        = var.yc_function_spec.description
  user_hash          = var.yc_function_spec.user_hash
  runtime            = var.yc_function_spec.runtime
  entrypoint         = var.yc_function_spec.entrypoint
  memory             = var.yc_function_spec.memory_mb
  execution_timeout  = var.yc_function_spec.execution_timeout_sec
  service_account_id = (
    var.yc_function_spec.create_service_account ?
    yandex_iam_service_account.function_service_account[0].id :
    null
  )
  tags               = var.yc_function_spec.tags
  content {
    zip_filename = "./data/functions/src/${var.yc_function_spec.source_zip}"
  }
}