output "function_id" {
  value = yandex_function.function.id
}

output "function_service_account_id" {
  value = (
    var.yc_function_spec.create_service_account ?
    yandex_iam_service_account.function_service_account[0].id :
    null
  )
}