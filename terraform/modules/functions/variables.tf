variable "yc_function_name" {
  description = "Name of function to provision"
  type        = string
}

variable "yc_function_spec" {
  description    = "Function specification details"
  type           = object({
    runtime                = string
    entrypoint             = string
    user_hash              = string
    source_zip             = string
    description            = optional(string, null)
    tags                   = optional(list(string), null)
    memory_mb              = optional(number, 128)
    execution_timeout_sec  = optional(number, 10)
    create_service_account = optional(bool, false)
    service_account_name   = optional(string, null)
  })
}