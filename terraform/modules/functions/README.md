# Yandex Cloud function module

A terraform module to provision functions in Yandex Cloud environment.

## Input variables
  - `yc_function_name` - Name for a function to create
  - `yc_function_spec` - Object of properties for specific function including runtime, handler, service account options, resources etc.

## Usage
Create a file for each function under /data/functions/function-0.yaml for example:
  ```yaml
  description: "Function 0"
  # To refresh function update user_hash value with some value
  # recommend to use version tag
  user_hash: "v0.1"
  tags: ["v0-1"]
  runtime: "python39"
  # Main python file dot python function name
  entrypoint: "function.handler"
  memory_mb: 128
  execution_timeout_sec: 10
  service_account_name: function-service-account-0
  # Source files must be packed to zip archive and placed under
  # data/functions/src folder
  source_zip: function-0.zip
  ```
Include module to parent:
  ```hcl
  module "functions" {
    for_each         = { for file in fileset("./data/functions", "*.yaml") : trimsuffix(file, ".yaml") => yamldecode(file("./data/functions/${file}")) }
    source           = "./modules/functions"
    yc_function_name = each.key
    yc_function_spec = each.value
  }
  ```

## Outputs
  - `function_id` - Id of created function
  - `function_service_account_id` - Id of created service account