# Get subnet ID
data "yandex_vpc_subnet" "subnet" {
  name = var.yc_instance_spec.subnet_name
}

# Get image name
data "yandex_compute_image" "image" {
  family = var.yc_instance_spec.image_family
}

resource "yandex_compute_instance" "instance" {
  name = var.yc_instance_name
  zone = data.yandex_vpc_subnet.subnet.zone

  resources {
    cores  = var.yc_instance_spec.cores
    memory = var.yc_instance_spec.memory
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.image.id
    }
  }

  network_interface {
    subnet_id = data.yandex_vpc_subnet.subnet.id
    nat       = var.yc_instance_spec.enable_nat
  }

  metadata = {
    user-data = (
      var.yc_instance_spec.user_data_file == null ?
      null :
      file("./data/instancies/user-data/${var.yc_instance_spec.user_data_file}")
    )
  }
}