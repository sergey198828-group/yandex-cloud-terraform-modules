# Yandex Cloud Instancies module

A terraform module to provision compute instancies in Yandex Cloud environment.

## Input variables
  - `yc_instance_name` - Name for an instance to create
  - `yc_instance_spec` - Object of properties for specific instance CPU, memory, image, subnet, user-data etc. (Please note: zone is automatically inherited from subnet)

## Usage
Create a file for each instance under /data/instancies/instance-0.yaml for example:
  ```yaml
  image_family: ubuntu-2204-lts
  subnet_name: network-0-subnet-0
  user_data_file: instance-user-data-0.yaml
  ```
Include module to parent:
  ```hcl
  module "instancies" {
    for_each         = { for file in fileset("./data/instancies", "*.yaml") : trimsuffix(file, ".yaml") => yamldecode(file("./data/instancies/${file}")) }
    source           = "./modules/instancies"
    yc_instance_name = each.key
    yc_instance_spec = each.value
    depends_on = [
      module.vpcs
    ]
  }
  ```

## Outputs
  - `instance_id` - Id of created instance
  - `internal_ip_address` - Internal IP address of created instance
  - `external_ip_address` - Public IP address of created instance