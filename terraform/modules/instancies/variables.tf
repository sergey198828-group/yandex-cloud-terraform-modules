variable "yc_instance_name" {
  description = "Name of instance to provision"
  type        = string
}

variable "yc_instance_spec" {
  description = "Instance specification details"
  type             = object({
    image_family   = string
    subnet_name    = string
    cores          = optional(number, 2)
    memory         = optional(number, 2)
    enable_nat     = optional(bool, false)
    user_data_file = optional(string, null)
  })
}