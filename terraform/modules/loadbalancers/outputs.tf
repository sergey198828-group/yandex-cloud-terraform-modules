output "listener_external_ip_address" {
  value = {
    for k, v in yandex_lb_network_load_balancer.loadbalancer.listener : k.name => v.external_address_spec
  }
}