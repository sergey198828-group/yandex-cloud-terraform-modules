data "yandex_vpc_subnet" "target_subnet" {
  for_each = var.yc_loadbalancer_spec.targets
  name = each.value.subnet_name
}

data "yandex_compute_instance" "target_instance" {
  for_each = var.yc_loadbalancer_spec.targets
  name = each.value.instance_name
}

resource "yandex_lb_target_group" "target_group" {
  name = "${var.yc_loadbalancer_name}-target-group"

  dynamic "target" {
    for_each = var.yc_loadbalancer_spec.targets
    content {
      subnet_id = data.yandex_vpc_subnet.target_subnet[target.key].id
      address   = data.yandex_compute_instance.target_instance[target.key].network_interface.0.ip_address
    }
  }
}

resource "yandex_lb_network_load_balancer" "loadbalancer" {
  name = var.yc_loadbalancer_name

  dynamic "listener" {
    for_each = var.yc_loadbalancer_spec.listeners
    content {
      name        = listener.key
      port        = listener.value.port
      target_port = listener.value.target_port
      protocol    = listener.value.protocol
      external_address_spec {
        ip_version = "ipv4"
      }
    }
  }

  attached_target_group {
    target_group_id = yandex_lb_target_group.target_group.id

    dynamic "healthcheck"{
      for_each = var.yc_loadbalancer_spec.healthchecks
      content {
        name                = healthcheck.key
        interval            = healthcheck.value.interval
        timeout             = healthcheck.value.timeout
        unhealthy_threshold = healthcheck.value.unhealthy_threshold
        healthy_threshold   = healthcheck.value.healthy_threshold
        tcp_options {
          port = healthcheck.value.tcp_port
        }
      }
    }
  }
}