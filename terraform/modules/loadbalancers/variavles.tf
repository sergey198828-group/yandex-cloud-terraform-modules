variable "yc_loadbalancer_name" {
  type = string
  description = "Loadbalancer name to create"
}

variable "yc_loadbalancer_spec" {
  type = object({
    targets = map(object({
      subnet_name   = string
      instance_name = string
    }))
    listeners = map(object({
      port        = number
      target_port = number
      protocol    = optional(string, "TCP")
    }))
    healthchecks = map(object({
      interval            = number
      timeout             = number
      unhealthy_threshold = number
      healthy_threshold   = number
      tcp_port            = number
    }))
  })
  description = "Loadbalancer specification to create"
}