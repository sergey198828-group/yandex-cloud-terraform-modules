# Yandex Cloud LoadBalancers module

A terraform module to provision loadbalancers in Yandex Cloud environment.

## Input variables
  - `yc_loadbalancer_name` - Name of loadbalancer to create
  - `yc_loadbalancer_spec` - Object with properties including targets, listeners and healthchecks.

## Usage
Create a file for each loadbalancer under /data/loadbalancers like loadbalancer-0.yaml for example:
  ```yaml
  targets:
    loadbalancer-0-target-0:
      subnet_name: network-0-subnet-0
      instance_name: instance-0
    loadbalancer-0-target-1:
      subnet_name: network-0-subnet-1
      instance_name: instance-0
  listeners:
    ssh-listener-0:
      port: 22
      target_port: 22
      protocol: tcp
  healthchecks:
    ssh-healthcheck-0:
      interval: 2
      timeout: 2
      unhealthy_threshold: 2
      healthy_threshold: 2
      tcp_port: 22
  ```
Include module to parent:
  ```hcl
  module "loadbalancers" {
    for_each = {
      for file in fileset("./data/loadbalancers", "*.yaml") : trimsuffix(file, ".yaml") => yamldecode(file("./data/loadbalancers/${file}"))
    }
    source               = "./modules/loadbalancers"
    yc_loadbalancer_name = each.key
    yc_loadbalancer_spec = each.value
    depends_on = [
      module.vpcs,
      module.instancies
    ]
  }
  ```

## Outputs
  - `listener_external_ip_address` - Created listeners external IPs