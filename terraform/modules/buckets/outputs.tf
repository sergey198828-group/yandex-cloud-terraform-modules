output "bucket_id" {
  value = yandex_storage_bucket.object-storage.id
}

output "storage_service_account_id" {
  value = (
    var.yc_bucket_spec.create_service_account ?
    yandex_iam_service_account.storage_service_account[0].id :
    null
  )
}