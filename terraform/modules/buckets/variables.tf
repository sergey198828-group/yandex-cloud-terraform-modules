variable "yc_bucket_name" {
  description = "Name of bucket to provision"
  type        = string
}

variable "yc_bucket_spec" {
  description    = "Bucket specification details"
  type           = object({
    zone                     = string
    folder_name              = string
    create_service_account   = optional(bool, false)
    create_static_access_key = optional(bool, false)
    service_account_name     = optional(string, "")
    max_size                 = optional(number, 1073741824)
  })
}