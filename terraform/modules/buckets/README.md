# Yandex Cloud bucket module

A terraform module to provision storage buckets in Yandex Cloud environment.

## Input variables
  - `yc_bucket_name` - Name for a bucket to create
  - `yc_bucket_spec` - Object of properties for specific bucket including size, folder, service account options, zone etc.

## Usage
Create a file for each bucket under /data/buckets/bucket-0.yaml for example:
  ```yaml
  create_service_account: true
  create_static_access_key: true
  service_account_name: bucket-service-account-0
  folder_name: terraform-test
  max_size: 1073741824
  zone: "ru-central1-a"
  ```
Include module to parent:
  ```hcl
  module "buckets" {
    for_each       = { for file in fileset("./data/buckets", "*.yaml") : trimsuffix(file, ".yaml") => yamldecode(file("./data/buckets/${file}")) }
    source         = "./modules/buckets"
    yc_bucket_name = each.key
    yc_bucket_spec = each.value
  }
  ```

## Outputs
  - `bucket_id` - Id of created bucket
  - `storage_service_account_id` - Id of created service account