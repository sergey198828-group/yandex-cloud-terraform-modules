data "yandex_resourcemanager_folder" "folder" {
  name     = var.yc_bucket_spec.folder_name
}

resource "yandex_iam_service_account" "storage_service_account" {
  count = var.yc_bucket_spec.create_service_account ? 1 : 0
  name  = var.yc_bucket_spec.service_account_name
}

resource "yandex_resourcemanager_folder_iam_member" "storage_service_account-editor" {
  count     = var.yc_bucket_spec.create_service_account ? 1 : 0
  folder_id = data.yandex_resourcemanager_folder.folder.id
  role      = "storage.editor"
  member    = "serviceAccount:${yandex_iam_service_account.storage_service_account[0].id}"
}

resource "yandex_iam_service_account_static_access_key" "storage_service_account-static-key" {
  count              = (
    var.yc_bucket_spec.create_service_account && var.yc_bucket_spec.create_static_access_key ?
    1 :
    0
  )
  service_account_id = yandex_iam_service_account.storage_service_account[0].id
  description        = "static access key for bucket"
}

resource "yandex_storage_bucket" "object-storage" {
  access_key = (
    var.yc_bucket_spec.create_service_account && var.yc_bucket_spec.create_static_access_key ?
    yandex_iam_service_account_static_access_key.storage_service_account-static-key[0].access_key :
    null
  )
  secret_key = (
    var.yc_bucket_spec.create_service_account && var.yc_bucket_spec.create_static_access_key ?
    yandex_iam_service_account_static_access_key.storage_service_account-static-key[0].secret_key :
    null
  )
  folder_id  = data.yandex_resourcemanager_folder.folder.id
  bucket     = var.yc_bucket_name
  max_size   = var.yc_bucket_spec.max_size
}