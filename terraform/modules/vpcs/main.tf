resource "yandex_vpc_network" "network" {
  name = var.yc_network_name
}

resource "yandex_vpc_subnet" "subnet" {
  for_each       = var.yc_network_subnets
  name           = each.key
  network_id     = yandex_vpc_network.network.id
  zone           = each.value.zone
  v4_cidr_blocks = each.value.v4_cidr_blocks
}
