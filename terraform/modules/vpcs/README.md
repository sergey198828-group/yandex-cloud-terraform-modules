# Yandex Cloud VPCs module

A terraform module to provision network and subnets in Yandex Cloud environment.

## Input variables
  - `yc_network_name` - Name for a network to create
  - `yc_network_subnets` - List of subnets with all details including name, zone and v4_cidr_blocks list

## Usage
Create a file for each network under /data/vpcs like network-0.yaml for example:
  ```yaml
  - name: network-0-subnet-0
    zone: "ru-central1-a"
    v4_cidr_blocks:
      - "192.168.0.0/24"
  - name: network-0-subnet-1
    zone: "ru-central1-b"
    v4_cidr_blocks:
      - "192.168.1.0/24"
  ```
Include module to parent:
  ```hcl
  module "vpcs" {
    for_each           = { for file in fileset("./data/vpcs", "*.yaml") : trimsuffix(file, ".yaml") => yamldecode(file("./data/vpcs/${file}")) }
    source             = "./modules/vpcs"
    yc_network_name    = each.key
    yc_network_subnets = each.value
  }
  ```

## Outputs
  - `network_id` - Id of created network
  - `subnet_ids` - Ids of created subnets