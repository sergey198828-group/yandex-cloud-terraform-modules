variable "yc_network_name" {
  type = string
  description = "Network name to create"
}

variable "yc_network_subnets" {
  type = map(object({
    zone           = string
    v4_cidr_blocks = list(string)
  }))
  description = "Subnets to create details"
}