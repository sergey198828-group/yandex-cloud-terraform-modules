# Yandex cloud terraform modules

This repository contains expandable templates to provision infrastructure in Yandex Cloud using terraform.

## Usage
  - Follow the instructions to setup terraform with Yandex Cloud provider. [General guide](https://cloud.yandex.com/en/docs/tutorials/infrastructure-management/terraform-quickstart) [Remote storage for state guide](https://cloud.yandex.com/en/docs/tutorials/infrastructure-management/terraform-state-storage)
  - Populate /terraform/data folder with nessesary resources details
  - Run commands:
  ```bash
  cd terraform
  terraform init \
  -backend-config="bucket=${YC_TF_STATE_BUCKET}" \
  -backend-config="key=${YC_TF_STATE_FILE}" \
  -reconfigure
  terraform fmt
  terraform validate
  terraform plan -out plan
  terraform apply plan
  ```

## Pipeline
  Project has CI/CD pipeline setup which automatically check terraform and data files, plan and apply configuration and then destroy.
  Pipeline expects certain environment variables to be setup:  
  - `YC_CLOUD_ID` - Id of your cloud in Yandex
  - `YC_FOLDER_ID` - Id of your folder inside cloud
  - `YC_SERVICE_ACCOUNT_KEY_FILE` - A key file to manage cloud
  - `YC_TF_STATE_BUCKET` - S3 storage bucket to store terraform tfstate
  - `YC_TF_STATE_FILE` - Path to the file with state in bucket
  - `AWS_REGION` - Yandex region, most probably ru-central1
  - `AWS_ACCESS_KEY_ID` - S3 storage key id
  - `AWS_SECRET_ACCESS_KEY` - S3 storage secret